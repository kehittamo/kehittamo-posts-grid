<?php
/*
Plugin Name: Kehittämö Posts Grid
Plugin URI: http://www.kehittamo.fi
Description: Display posts in beautiful responsive grid
Version: 0.9.0
Author: Mikko Virenius, Janne Saarela, Jani Krunniniva
Author Email: asiakaspalvelu@kehittamo.fi
License: GPL2

  Copyright 2011 Mikko Virenius (mikko.virenius@kehittamo.fi)
  Copyright 2015 Janne Saarela (janne.saarela@kehittamo.fi)
  Copyright 2015 Jani Krunniniva (jani.krunniniva@kehittamo.fi)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


namespace Kehittamo\Plugins\PostsGrid;

	define( 'Kehittamo\Plugins\PostsGrid\PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
	define( 'Kehittamo\Plugins\PostsGrid\PLUGIN_URL', plugin_dir_url( __FILE__ ) );

	class Load{

    /**
     * Construct the plugin and load dependencies
     */
    function __construct(){

      add_action( 'plugins_loaded', array( $this, 'load_plugin' ));

      add_action( 'plugins_loaded', array( $this, 'register_acf' ) );

      add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

      add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ), 999 );

    }

    /**
     * Load the plugin and its dependencies
     */
    function load_plugin(){

        add_action( 'widgets_init', array($this,'widget' ));

        add_filter( 'script_loader_tag', array( $this, 'add_defer_attribute' ), 10, 2 );

        $this->load_textdomain();

        $this->admin();

    }

		/**
		* Load plugin textdomain.
		*/
		function load_textdomain() {

			load_plugin_textdomain( 'kehittamo-posts-grid', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		}


    /**
     * Plugin admin page
     */
    function admin(){

       // Load plugin options page
      require_once( PLUGIN_PATH . '/includes/kehittamo-posts-grid-admin.php' );

    }


    /**
     * Posts Grid Widget
     */
    function widget(){

      // Load wiget
      require_once( PLUGIN_PATH . '/includes/kehittamo-posts-grid-widget.php' );

      // Register the widget
      register_widget( 'Kehittamo\Plugins\PostsGrid\Widget' );

    }

    /**
     * Load Frontend Styles
     */

    function wp_enqueue_scripts(){

      $options = get_option( 'kehittamo_posts_grid_settings' );

      if( ! is_admin() AND isset( $options['global_styles'] ) AND $options['global_styles'] == 1 ) {

        //CSS Styles
        wp_register_style( 'kehittamo-posts-grid-frontend', PLUGIN_URL .'includes/css/kehittamo-posts-grid-frontend.min.css' );
        wp_enqueue_style( 'kehittamo-posts-grid-frontend' );

      }

      if( ! is_admin() ) {
        // TODO: make this work!
        // if( wp_script_is( 'kehittamo-icons',  'registered' ) ) :
          wp_register_style( 'kehittamo-posts-grid-web-font', PLUGIN_URL .'includes/css/kehittamo-posts-grid.css' );
          wp_enqueue_style( 'kehittamo-posts-grid-web-font' );
        // endif;
        wp_register_script( 'kehittamo-posts-grid-picturefill', PLUGIN_URL .'includes/js/picturefill.min.js' );
        wp_enqueue_script( 'kehittamo-posts-grid-picturefill' );
      }

    }

    function add_defer_attribute( $tag, $handle ) {
        if ( 'kehittamo-posts-grid-picturefill' !== $handle ) return $tag;
        return str_replace( ' src', ' async="async" src', $tag );
    }

    /**
     * Load Admin Javascript and Styles
     */
    function admin_enqueue_scripts( $hook ){

      if( is_admin() AND 'widgets.php' == $hook ){

        // Javascript
        wp_register_script( 'kehittamo-posts-grid', PLUGIN_URL .'includes/js/kehittamo-posts-grid.min.js', array( 'backbone', 'underscore','jquery', 'wp-color-picker','jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-accordion', 'jquery-ui-autocomplete', 'jquery-ui-tabs', 'jquery-ui-sortable', 'jquery-ui-draggable', 'jquery-ui-droppable' ),'1.0', true);
        wp_enqueue_script( 'kehittamo-posts-grid' );

        $posts_arr = $this->get_posts();
        $data = $this->get_collection_items();

        wp_localize_script( 'kehittamo-posts-grid', 'KehittamoPostsGridHelper', array( 'posts' => $posts_arr, 'ajax_url' => admin_url( 'admin-ajax.php' ) , 'data' => $data ));


        // CSS Styles
        wp_enqueue_style( 'jquery-ui', 'https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' );
        wp_register_style( 'kehittamo-posts-grid-admin', PLUGIN_URL .'includes/css/kehittamo-posts-grid-admin.min.css' );
        wp_enqueue_style( 'kehittamo-posts-grid-admin' );
        wp_enqueue_style( 'wp-color-picker' );

      }


    }

    /**
     * Register AJAX action handlers
     */
    function ajax_actions(){

      add_action( 'wp_ajax_get_collection_items', array( $this, 'get_collection_items' ) );

    }

    /**
     * Get all widget settings
     */
    function get_collection_items(){

        $data = array();
        $widgets = get_option( 'widget_posts_grid' );

		if( ! $widgets )
			return false;

        foreach( $widgets as $widget_id => $widget_data ){

          $numbers = explode( ', ', $widget_data['numbers'] );

          if( $numbers ) {

            foreach( $numbers as $id ){

              $post = get_post($id);

							if( $post ){

								$single = array();
								$single['title'] = $post->post_title;
								$single['id'] = $post->ID;

	              if( $single['title'] AND $single['id'] )
	                $data[$widget_id][] = $single;

							}

            }

          }

        }

        return $data;

    }

    /**
     * Get all posts
     */
    function get_posts(){

      $cache = get_transient( '_autocomplete_posts_array' );

      if( $cache ) {
        return $cache;
      } else{

        $posts = get_posts( 'posts_per_page=100' );

        $posts_arr = array();

        foreach ($posts as $post) {
          $single = array();
          $single['label'] = $post->post_title;
          $single['value'] = $post->ID;

          $posts_arr[] = $single;

        }

        set_transient( '_autocomplete_posts_array', $posts_arr, 60);

        return $posts_arr;

      }


    }


    /**
     * Register and add Advaced Custom Fields Settings
     */
    public function register_acf() {

        if ( function_exists( "register_field_group" ) ) {

            register_field_group(array (
                'id' => 'acf_articles',
                'title' => __( 'Artikkelit', 'kehittamo-posts-grid' ),
                'fields' => array (
                    array (
                        'key' => 'field_video_embed',
                        'label' => __( 'Videon upotuskoodi', 'kehittamo-posts-grid' ),
                        'name' => 'video_embed',
                        'type' => 'text',
                        'instructions' => __( 'Laita tähän videon jako-osoite, eli se osoite, jonka saat kun klikkaat "Jaa" Youtuben videosivulla. Jos kenttä ei ole tyhjä, video korvaa kuvan grid-näkymässä sekä artikkelinäkymässä, jos Artikkelimuoto on video.', 'kehittamo-posts-grid' ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'post',
                            'order_no' => 0,
                            'group_no' => 0,
                        ),
                    ),
                ),
                'options' => array (
                    'position' => 'normal',
                    'layout' => 'no_box',
                    'hide_on_screen' => array (
                        0 => 'permalink',
                        1 => 'discussion',
                        2 => 'revisions',
                        3 => 'send-trackbacks',
                    ),
                ),
                'menu_order' => 999
            ));
        }

    }

  }

  $kehittamo_posts_grid = new \Kehittamo\Plugins\PostsGrid\Load();
