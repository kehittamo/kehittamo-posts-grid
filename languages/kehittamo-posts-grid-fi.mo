��    '      T  5   �      `     a     o     ~     �     �  	   �     �     �     �     �  E   �  C   0     t     �     �  	   �  D   �     �     �     �  .   	     8  
   @     K     _     q          �     �     �     �     �     �     �     �     �            �       �     �     �     	     	  
   	     %	     -	     6	     F	  j   `	  8   �	  ,   
     1
     C
     U
  R   m
  
   �
     �
     �
  4   �
  
   !     ,     >     Z  	   l     v          �     �     �     �     �     �                    0     %                                               "      $             &          
                                    #                                       '           	      !    Add new post: All categories Alternately Left and Right Background color: Bottom Category: Circle Columns: Custom order: Disable overlay Enter the number of characters, after which the excerpt is being cut. Enter the number of characters, after which the title is being cut. Excerpt Length: General settings Hide category Hide date Just start typing or press space bar. Remember to save your changes. Left None Normal (4:4) Number of posts to skip from the start (0-20). Offset: Posts Grid Posts Grid Settings Posts Grid Widget Recent posts: Right Rows: Show share counts Text color: This is full width element Thumbnail Position: Thumbnail Size: Title Length: Title: Top Use Global Styles Wide (16:9) Project-Id-Version: Adian Posts Grid
POT-Creation-Date: 2015-10-07 11:43+0300
PO-Revision-Date: 2015-10-07 11:45+0300
Last-Translator: Janne Saarela <janne.saarela@kehittamo.fi>
Language-Team: Mikko Virenius <mikko.virenius@adian.fi>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Lisää uusi kirjoitus: Kaikki kategoriat Vuorotellen vasen ja oikea Taustaväri: Alhaalla Kategoria: Ympyrä Palstat: Oma järjestys: Siirrä teksti kuvan alle Anna merkkimäärä, jonka jälkeen ote katkaistaan. Jätä tyhjäksi, mikäli et halua näyttää otetta. Anna merkkimäärä, jonka jälkeen otsikko katkaistaan. Kuinka monta merkkiä otteesta näytetään? Yleiset asetukset Piilota kategoria Piilota päivämäärä Aloita kirjoittaminen tai paina välilyöntiä. Muista lopuksi tallentaa muutokset Vasemmalla Älä näytä kuvaa Neliö (4:4) Kuinka monta kirjoitusta jätetään väliin (0-20). Siirtymä: Kirjoitusruudukko Kirjoitusruudukon asetukset Kirjoitusruudukko Uusimmat: Oikealla Rivit: Näytä jakotilastot Tekstin väri: Tämä on täysleveä elementti Kuvan sijainti: Kuvan koko/muoto: Otsikon maksimipituus: Otsikko Ylhäällä Käytä lisosan tyylejä Leveä (16:9) 