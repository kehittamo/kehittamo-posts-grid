function initColorPicker(widget) {
  widget.find( '.posts-grid-color-picker' ).wpColorPicker({
    change: _.throttle(function() { // For Customizer
      jQuery(this).trigger( 'change' );
    }, 3000)
  });
}

jQuery(document).ready(function($) {
  // Check if posts-grid-color-picker elements are active
  if ($( '.posts-grid-color-picker' ).length > 0) {
    $( '#widgets-right .widget:has(.posts-grid-color-picker)' ).each(function() {
      initColorPicker($(this));
    });
    // Add Color Picker to all inputs that have 'posts-grid-color-picker' class inside .widget-liquid-right
    // $( '.widget-liquid-right .posts-grid-color-picker' ).wpColorPicker();
    // Add ajaxComplete event listener so we can re-run wp color picker
    // TODO: this can slow down the widgets.php page -> better solution
    // $(document).ajaxComplete(function( event, xhr, settings ) {
    //   if ( settings.url === "/wp-admin/admin-ajax.php" ) {
    //     // Add Color Picker to all inputs that have 'posts-grid-color-picker' class inside .widget-liquid-right
    //     $( '.widget-liquid-right .posts-grid-color-picker' ).wpColorPicker();
    //   }
    // });
  }

  function PostsGrid() {

    /* MODELS */

    var PostModel = Backbone.Model.extend({
      validate: function(attrs, options) {
        if (attrs.title.length < 3) {
          return "can't end before it starts";
        }
      }
    });

    var PostsCollection = Backbone.Collection.extend({

      model: PostModel,
      comparator: function(model) {
        return model.get( 'ordinal' );
      },

      initialize: function() {}

    });

    var AutocompleteModel = Backbone.Model.extend({
      label: function() {
        return this.get("label");
      }
    });

    var AutocompleteCollection = Backbone.Collection.extend({
      model: AutocompleteModel
    });


    // This view turns a Service model into HTML. Will create LI elements.
    var ItemView = Backbone.View.extend({
      tagName: 'li',
      className: 'item-view',

      events: {
        'drop': 'drop',
        'click span': 'remove'
      },

      remove: function() {
        this.$el.trigger( 'deleteFromCollection', [this.model]);
      },

      drop: function(event, index) {
        this.$el.trigger( 'updateCollection', [this.model, index]);
      },

      initialize: function() {

        // Set up event listeners. The change backbone event
        // is raised when a property changes (like the checked field)

        this.listenTo(this.model, 'change', this.render);
      },

      render: function() {

        // Create the HTML

        this.$el.html( '<dl class="menu-item-bar ' + this.model.get( 'status' ) + '"><dt class="menu-item-handle"><span class="item-title">' + this.model.get( 'title' ) + '</span><span class="item-controls"><a class="item-edit">Poista</a></span></dt></dl>' );

        // Returning the object is a good practice
        // this makes chaining possible
        return this;
      }
    });

    // The main view of the application
    var ListView = Backbone.View.extend({

      tagname: 'div',

      events: {
        'updateCollection': 'updateSort',
        'deleteFromCollection': 'removeItem',
        'change .radio': 'showHide',
        'change .columns': 'render',
        'change .rows': 'render',
        'focus #addnew': 'getAutocomplete',
      },


      showHide: function(e) {
        var order = this.$( '.order:checked' ).val() || 'recent';

        if (order == 'custom' ) {
          this.$.addform.show();
          this.$.category.find( '.category' ).val( '' );
          this.$.category.hide();
        } else {
          this.$.addform.hide();
          this.$.category.show();
        }
      },

      getLimit: function() {
        return this.$.maxrows.val() * this.$.maxcolumns.val();
      },

      initialize: function(options) {

        this.setElement( '#kehittamo_posts_grid-' + this.id);

        if (typeof (KehittamoPostsGridHelper.data[this.id]) == 'undefined' )
          KehittamoPostsGridHelper.data[this.id] = [];

        if (!this.collection)
          this.collection = new PostsCollection(KehittamoPostsGridHelper.data[this.id]);

        this.$.addform = this.$( '.addform' );
        this.$.maxrows = this.$( '.rows' );
        this.$.maxcolumns = this.$( '.columns' );
        this.$.category = this.$( '.category-wrapper' );
        this.$.list = this.$( '.posts-container' );
        this.$.autocomplete = this.$( '.add' );

        this.renderOnSave();

        this.getAutocomplete();

        // Listen for the change event on the collection.
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'add', this.render);
        this.listenTo(this.collection, 'remove', this.render);


        //this.getAutocomplete();

        this.render();

        /**
         * Sorting
         */
        this.$.list.sortable({
          // consider using update instead of stop
          stop: function(event, ui) {
            ui.item.trigger( 'drop', ui.item.index());
          }
        });

      },


      render: function() {


        var self = this;
        var limit = this.getLimit();


        this.showHide();

        this.$.list.empty();

        this.collection.each(function(item, index) {

          index = index + 1;

          if (index > limit)
            item.set( 'status', 'inactive' );
          else
            item.set( 'status', 'active' );

          var view = new ItemView({
            model: item
          });

          self.$.list.append(view.render().el);


        }); // "this" is the context in the callback

        return this;
      },


      addItem: function(id, title) {

        this.$.autocomplete.val( '' );

        var model = new PostModel({
          title: title,
          id: id
        });

        this.collection.add(model);

        KehittamoPostsGridHelper.data[this.id].push({
          title: model.get( 'title' ),
          id: model.get( 'id' )
        });

        var ids = this.collection.pluck( 'id' );


        this.$( '#ids' ).val(ids.join( ', ' ));

      },

      removeItem: function(event, model) {

        this.collection.remove(model);

        KehittamoPostsGridHelper.data[this.id] = _.filter(KehittamoPostsGridHelper.data[this.id], function(item) {
          return item.id !== model.get( 'id' );
        });

        var ids = this.collection.pluck( 'id' );
        this.$( '#ids' ).val(ids.join( ', ' ));


      },

      updateSort: function(event, model, position) {

        var self = this;

        this.collection.remove(model);

        this.collection.each(function(model, index) {
          var ordinal = index;
          if (index >= position)
            ordinal += 1;
          model.set( 'ordinal', ordinal);

        });

        model.set( 'ordinal', position);

        this.collection.add(model, {
          at: position
        });

        // to update ordinals on server:
        var ids = this.collection.pluck( 'id' );
        var plucked = this.collection.invoke("pick", ["id", "title"]);

        KehittamoPostsGridHelper.data[this.id] = plucked;

        this.$( '#ids' ).val(ids.join( ', ' ));

        this.render();

      },


      getAutocomplete: function() {

        /**
         * Autocomplete
         */

        if (this.$.autocomplete.length > 0) {

          var self = this;

          this.$.autocomplete.autocomplete({
            minLength: 0,
            source: KehittamoPostsGridHelper.posts,
            focus: function(event, ui) {
              self.$.autocomplete.val(ui.item.label);
              return false;
            },
            select: function(event, ui) {
              self.$.autocomplete.val( '' );
              self.addItem(ui.item.value, ui.item.label);


              return false;
            }
          })
            .data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li>")
              .append("<a>" + item.label + "</a>")
              .appendTo(ul);
          };
        }




      },


      renderOnSave: function() {

        var self = this;

        jQuery(document).ajaxComplete(function(e, xhr, settings) {

          // determine which ajax request is this (we're after "save-widget")
          var request = {},
            pairs = settings.data.split( '&' ),
            i, split, widget;
          for (i in pairs) {

            if (typeof (pairs[i]) == 'string' ) {
              split = pairs[i].split( '=' );
              request[decodeURIComponent(split[0])] = decodeURIComponent(split[1]);
            }

          }

          // only proceed if this was a widget-save request
          if (request.action && (request.action === 'save-widget' ) && (self.id == request.multi_number || self.id == request.widget_number)) {


            if (request.multi_number.length > 0) {
              var id = request.multi_number;
            }

            if (typeof (id) == 'undefined' && request.widget_number.length > 0) {
              var id = request.widget_number;
            }

            if (id.length > 0) {
              new ListView({
                id: id
              });
            }



          }

        });

      }


    });



    this.start = function() {

      $(document.body).on( 'click.widget', function(e) {

        var target = $(e.target).closest( '.widget' );

        var widget = target.find( '.kehittamo-posts-grid-widget' );

        var id = widget.data( 'widget-id' );

        if (id != null && id.length == 0) {
          id = widget.attr( 'id' ).replace(/\D+/, '' );
        }

        if (typeof (id) != 'undefined' && id != null) {
          new ListView({
            id: id
          });
        }

      });



    };


  } // End of APP


  new PostsGrid().start();


});
