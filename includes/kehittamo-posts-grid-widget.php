<?php

namespace Kehittamo\Plugins\PostsGrid;

use \WP_Widget;
use \WP_Query;

/**
 * Adds Posts_Grid_Widget widget.
 */
class Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'posts_grid', // Base ID
            __( 'Posts Grid', 'kehittamo-posts-grid' ), // Name
            array( 'description' => __( 'Posts Grid Widget', 'kehittamo-posts-grid' ), ) // Args
        );
        // add shortcodes
        add_shortcode( 'kirjoitusruudukko', array( $this, 'kirjoitusruudukko_func' ));
    }

    /**
     * kehittamo-posts-grid shortcode
     *
     *  category id (pakollinen tieto, jos shortcodessa ei ole määritelty, ei tulosteta mitään)
     *  columns: 1-4 (oletus 1)
     *  rows: 1-4 (oletus 1)
     *  thumbnail_position: top/bottom/left/right/alternately (oletus: top)
     *  thumbnail_size: wide/square/circle (oletus ei kuvaa eli "empty string")
     *
     * Example:
     * [kirjoitusruudukko category="1" columns="2" rows="2"  thumbnail_position="left" thumbnail_size="square"]
     */
    function kirjoitusruudukko_func( $atts ) {
        $atts = shortcode_atts(
            array(
                'category' => '',
                'columns' => '1',
                'rows' => '1',
                'thumbnail_position' => 'top',
                'thumbnail_size' => '',
                'background_color'  => ''
            ), $atts, 'kirjoitusruudukko' );

        if( $atts['thumbnail_size'] === '' ) {
            $atts['thumbnail_size'] = 'none';
        }

        $atts['order'] = 'recent';
        $atts['text_color'] = '#FFF';

        if( $atts[category] === '' ) {
            return __( "Kirjoitusruudukko: missing category", "kehittamo-posts-grid" );
        }

        return $this->get_posts_grid_data($atts);
    }
    // TODO: add css class logic from function widget
    private function get_posts_grid_data( $atts ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $title_length = (int) $instance['title_length'];
        $excerpt_length =  $instance['excerpt_length'] ? intval($instance['excerpt_length']) : '';
        //$columns = (int) $instance['columns'];
        $columns = (int) $atts['columns'];
        //$rows = (int) $instance['rows'];
        $rows = (int) $atts['rows'];
        $disable_overlay = esc_attr($instance['disable_overlay']);
        // If text is over image, set bg-color transparency
        $background_color = $disable_overlay ? $this->hex2rgb( $atts['background_color']) : $this->hex2rgb( $atts['background_color'], '0.25' );
        $text_color = $this->hex2rgb( $atts['text_color'] );
        $custom_css_bg = $background_color ? "background: $background_color;" : false;
        $custom_css_color = $text_color ? "color: $text_color;" : false;
        $hide_date = esc_attr($instance['hide_date']);
        $hide_category = esc_attr($instance['hide_category']);
        $full_width_grid = esc_attr($instance['full_width_grid']);
        $show_share_counts = esc_attr($instance['show_share_counts']);
        $thumbnail_size = esc_attr($atts['thumbnail_size']);
        //$thumbnail_size = esc_attr($instance['thumbnail_size']);
        $thumbnail_position = esc_attr($atts['thumbnail_position']);
        //$thumbnail_position = esc_attr($instance['thumbnail_position']);
        $offset = (int) $instance['offset'];
        $limit = (int) $columns*$rows;
        $category = (int) $atts['category'];
        //$category = (int) $instance['category'];
        $order = esc_attr($atts['order']);
        //$order = esc_attr($instance['order']);
        $posts_in = explode( ', ', esc_attr($instance['numbers']) );
        $desktop =  12 / $columns;
        $mobile = ( $columns > 1 ) ? 6 : 12 ;
        $default_image_size = ( $columns > 1 ) ? 'default-image-standard' : 'default-image';
        $placeholdit_url = ( $thumbnail_size === 'square' || $thumbnail_size === 'circle' ) ? 'http://placehold.it/200x200' : 'http://placehold.it/350x200';
        $kehittamo_options = get_option( 'kehittamo_posts_grid_settings' );
        $default_image_id = $this->get_attachment_id_by_url( $kehittamo_options['default_image_url'] );

        $defaults = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $limit
        );

        if( $order == 'recent' ) {
            $recent_args = array(   'cat' => $category, 'offset' => $offset);
            $query = wp_parse_args( $recent_args, $defaults );
        }

        if( $order == 'custom' ) {
            $custom_args = array( 'post__in' => $posts_in, 'orderby' => 'post__in' );
            $query = wp_parse_args( $custom_args, $defaults );
        }
        $recentPosts = new WP_Query($query);

        if( $thumbnail_position == 'left' OR $thumbnail_position == 'right' OR $thumbnail_position == 'alternately' ) :
            $custom_css_color = "color: #000";
            $disable_overlay='on';
        endif;

        if( $recentPosts->have_posts() ) : ?>
            <div class="posts-grid posts-grid-<?php echo $columns; ?>-columns <?php echo $thumbnail_position; ?>">
                <div class="row">
                    <?php while( $recentPosts->have_posts() ) : $recentPosts->the_post();
                        $classes = array();
                        if( has_post_thumbnail() )
                            $classes[] = 'has-post-thumbnail';

                        if( ! $disable_overlay ) {
                            $classes[] = 'overlay-enabled';
                            $classes[] = 'shortcut';
                        }

                        if( function_exists( 'get_field' ) AND get_field( 'is_column' ) )
                            $classes[] = 'type-column';
                    ?>
                    <div class="grid-block col-xs-<?php echo $mobile; ?> col-sm-<?php echo $mobile; ?> col-md-<?php echo $desktop; ?> <?php echo implode(" ", $classes); ?>" >

                    <a href="<?php the_permalink(); ?>">

                        <div class="grid-block-inner <?php echo $thumbnail_position;?>" >
                            <?php if( $thumbnail_size != 'none'  AND $thumbnail_position != 'bottom' ) : ?>
                               <div class="grid-block-picture">
                                <?php $this->add_thumbnail( $kehittamo_options, $default_image_id, $full_width_grid, $columns, $thumbnail_size, $placeholdit_url );

                                if( $show_share_counts ) : ?>
                                    <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                                <?php endif; ?>
                                </div>

                            <?php endif; ?>

                            <div class="grid-block-content" <?php if( $custom_css_bg ) : ?>style="<?php echo $custom_css_bg;?>" <?php endif;?>>
                                <?php if( ! $hide_date OR ! $hide_category ) : ?>
                                    <div class="grid-block-meta grid-block-byline">
                                        <?php if( ! $hide_date ) : ?>
                                            <div class="grid-block-date" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?>>
                                                <?php if($disable_overlay): ?><i class="icon icon-calendar"></i><?php endif; ?><?php the_time( 'd.m.Y' ); ?>
                                                <?php if($disable_overlay): ?><i class="icon icon-clock"></i><?php endif; ?><?php the_time( 'G:i' ); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if( ! $hide_category ) : ?>
                                            <div class="grid-block-categories">
                                                <?php echo $this->get_sub_category(); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>

                                <h<?php echo $columns; ?> class="grid-block-title" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?> >
                                    <?php echo $this->trim( get_the_title(), $title_length ); ?>
                                </h<?php echo $columns; ?>>
                            </div>

                            <?php if( $thumbnail_size != 'none' AND $thumbnail_position == 'bottom' ) : ?>
                                <div class="grid-block-picture">
                                <?php $this->add_thumbnail( $kehittamo_options, $default_image_id, $full_width_grid, $columns, $thumbnail_size, $placeholdit_url );

                                if( $show_share_counts ) : ?>
                                    <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                                <?php endif; ?>
                                </div>
                            <?php endif; ?>

                            <?php if( $excerpt_length > 0 ): ?>
                                <p><?php echo mb_substr( get_the_excerpt(), 0, $excerpt_length ) . '...'; ?></p>
                            <?php endif; ?>

                        </div>
                        </a>
                    </div>
                <?php  endwhile; ?>
                </div><!-- .row -->
            </div><!-- .posts-grid -->
        <?php endif; ?>
        <?php wp_reset_query();
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $title_length = (int) $instance['title_length'];
        $excerpt_length =  $instance['excerpt_length'] ? intval($instance['excerpt_length']) : '';
        $columns = (int) $instance['columns'];
        $rows = (int) $instance['rows'];
        $disable_overlay = esc_attr($instance['disable_overlay']);
        // If text is over image, set bg-color transparency
        $background_color = $disable_overlay ? $this->hex2rgb( $instance['background_color']) : $this->hex2rgb( $instance['background_color'], '0.5' );
        $background_color_xs = $this->hex2rgb( $instance['background_color']);
        $text_color = $this->hex2rgb( $instance['text_color'] );
        $custom_css_bg = $background_color ? "background: $background_color;" : false;
        $custom_css_bg_xs = $background_color_xs ? "background: $background_color_xs;" : false;
        $custom_css_color = $text_color ? "color: $text_color;" : false;
        $hide_date = esc_attr($instance['hide_date']);
        $hide_category = esc_attr($instance['hide_category']);
        $full_width_grid = esc_attr($instance['full_width_grid']);
        $show_share_counts = esc_attr($instance['show_share_counts']);
        $thumbnail_size = esc_attr($instance['thumbnail_size']);
        $thumbnail_position = esc_attr($instance['thumbnail_position']);
        $offset = (int) $instance['offset'];
        $limit = (int) $columns*$rows;
        $category = (int) $instance['category'];
        $order = esc_attr($instance['order']);
        $posts_in = explode( ', ', esc_attr($instance['numbers']) );
        $desktop = 12 / $columns;
        $mobile = ( $columns > 1 ) ? 6 : 12 ;
        $default_image_size = ( $columns > 1 ) ? 'default-image-standard' : 'default-image';
        $placeholdit_url = ( $thumbnail_size === 'square' || $thumbnail_size === 'circle' ) ? 'http://placehold.it/200x200' : 'http://placehold.it/350x200';
        $kehittamo_options = get_option( 'kehittamo_posts_grid_settings' );
        $default_image_id = isset( $kehittamo_options['default_image_url'] ) ? $this->get_attachment_id_by_url( $kehittamo_options['default_image_url'] ) : "";

        $defaults = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => $limit
        );

        echo $args['before_widget'];
        if ( ! empty( $title ) ) :
            echo $args['before_title'] . $title . $args['after_title'];
        endif;

        if( $order == 'recent' ) {
            $recent_args = array(   'cat' => $category, 'offset' => $offset);
            $query = wp_parse_args( $recent_args, $defaults );
        }

        if( $order == 'custom' ) {
            $custom_args = array( 'post__in' => $posts_in, 'orderby' => 'post__in' );
            $query = wp_parse_args( $custom_args, $defaults );
        }

        $recentPosts = new WP_Query($query);
        if( $recentPosts->have_posts() ) : ?>
            <div class="posts-grid container-fluid posts-grid-<?php echo $columns; ?>-columns <?php echo $thumbnail_position; ?>">
              <div class="row">
              <?php while( $recentPosts->have_posts() ) : $recentPosts->the_post();
                $classes = array($thumbnail_size);

                if( ( has_post_thumbnail() || $default_image_id ) && $thumbnail_size !== 'none' ){
                  $classes[] = 'has-post-thumbnail';
                }

                if( ! $disable_overlay ){
                  $classes[] = 'overlay-enabled';
                }

                if( function_exists( 'get_field' ) AND get_field( 'is_column' ) ){
                  $classes[] = 'type-column';
                }
                if( $show_share_counts ) {
                  $classes[] = 'share-counts-enabled';
                }

              ?>

              <div class="grid-block col-xs-<?php echo $mobile; ?> col-sm-<?php echo $mobile; ?> col-md-<?php echo $mobile; ?> col-lg-<?php echo $desktop; ?> <?php echo implode(" ", $classes); ?>" >

                  <a href="<?php the_permalink(); ?>">

                    <div class="grid-block-inner <?php echo $thumbnail_position;?>" >
                      <?php if( $thumbnail_size != 'none' AND $thumbnail_position != 'bottom' ) : ?>
                         <div class="grid-block-picture">
                          <?php $this->add_thumbnail( $kehittamo_options, $default_image_id, $full_width_grid, $columns, $thumbnail_size, $placeholdit_url );

                          if( $show_share_counts ) : ?>
                              <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                          <?php endif; ?>
                          </div>
                      <?php elseif( $show_share_counts AND $thumbnail_position != 'bottom' ) : ?>
                        <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                      <?php endif; ?>

                        <?php // Grid block content visible on size XS: No background alpha ?>
                        <div class="grid-block-content visible-xxs" <?php if( $custom_css_bg_xs ) : ?>style="<?php echo $custom_css_bg_xs;?>" <?php endif;?>>

                          <?php if( ! $hide_date OR ! $hide_category ) : ?>
                            <div class="grid-block-meta grid-block-byline">
                              <?php if( ! $hide_date ) : ?>
                                <div class="grid-block-date" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?>>
                                  <i class="icon icon-calendar"></i><?php the_time('d.m.Y'); ?>
                                  <i class="icon icon-clock"></i><?php the_time('G:i'); ?>
                                </div>
                              <?php endif; ?>
                              <?php if( ! $hide_category ) : ?>
                                <div class="grid-block-categories">
                                  <?php echo $this->get_sub_category(); ?>
                                </div>
                              <?php endif; ?>
                            </div>
                          <?php endif; ?>

                          <h<?php echo $columns; ?> class="grid-block-title" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?> >
                              <?php echo $this->trim( get_the_title(), $title_length ); ?>
                          </h<?php echo $columns; ?>>
                        <?php if( $excerpt_length > 0 ): ?>
                            <p <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?>><?php echo mb_substr( get_the_excerpt(), 0, $excerpt_length ) . '...'; ?></p>
                        <?php endif; ?>
                      </div>
                      <?php // End Grid block content ?>

                        <?php // Grid block content visible on size SM and up: background alpha ?>
                        <div class="grid-block-content hidden-xxs" <?php if( $custom_css_bg ) : ?>style="<?php echo $custom_css_bg;?>" <?php endif;?>>

                          <?php if( ! $hide_date OR ! $hide_category ) : ?>
                            <div class="grid-block-meta grid-block-byline">
                              <?php if( ! $hide_date ) : ?>
                                <div class="grid-block-date" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?>>
                                  <i class="icon icon-calendar"></i><?php the_time( 'd.m.Y' ); ?>
                                  <i class="icon icon-clock"></i><?php the_time( 'G:i' ); ?>
                                </div>
                              <?php endif; ?>
                              <?php if( ! $hide_category ) : ?>
                                <div class="grid-block-categories">
                                  <?php echo $this->get_sub_category(); ?>
                                </div>
                              <?php endif; ?>
                            </div>
                          <?php endif; ?>

                          <h<?php echo $columns; ?> class="grid-block-title" <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?> >
                              <?php echo $this->trim( get_the_title(), $title_length ); ?>
                          </h<?php echo $columns; ?>>
                        <?php if( $excerpt_length > 0 ): ?>
                            <p <?php if( $custom_css_color ) : ?>style="<?php echo $custom_css_color;?>" <?php endif;?>><?php echo mb_substr( get_the_excerpt(), 0, $excerpt_length ) . '...'; ?></p>
                        <?php endif; ?>
                      </div>
                      <?php // End Grid block content ?>

                      <?php if( $thumbnail_size != 'none' AND $thumbnail_position == 'bottom' ) : ?>
                        <div class="grid-block-picture">
                        <?php $this->add_thumbnail( $kehittamo_options, $default_image_id, $full_width_grid, $columns, $thumbnail_size, $placeholdit_url );

                        if( $show_share_counts ) : ?>
                            <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                        <?php endif; ?>
                        </div>
                      <?php elseif( $show_share_counts AND $thumbnail_position == 'bottom' ) : ?>
                        <?php echo $this->get_share_counts( get_the_ID(),  get_the_permalink(), false ); ?>
                      <?php endif; ?>

                    </div>
                  </a>
                </div>
              <?php  endwhile; ?>
              </div><!-- .row -->
            </div><!-- .posts-grid -->
        <?php endif; ?>
        <?php wp_reset_query();

        echo $args['after_widget'];
    }

    /**
     *  thumbnail image
     *
     */
    private function add_thumbnail( $kehittamo_options, $default_image_id, $full_width_grid, $columns, $thumbnail_size, $placeholdit_url ) {
        // Use video embed instead of picture if video embed url is given
        if ( $this->get_video_embed() ) {
            echo $this->get_video_embed();
        }
        elseif ( has_post_thumbnail() ) {
            // IMAGE HAS SET ON POST
            echo $this->get_widget_image( get_the_ID(), $columns, $thumbnail_size, $full_width_grid );
        }
        elseif( isset( $kehittamo_options['default_image_url'] ) && is_null($default_image_id)) {
            // IMAGE HAS SET BUT IT DOESNT FOUND IN DATABASE USE DEFAULT IMAGE
            echo '<picture class="'.$thumbnail_size.'">' .
                    '<img src="'. $kehittamo_options['default_image_url'] .'"/>'.
                '</picture>';
        }
        elseif( $default_image_id ) {
            // DEFAULT IMAGE HAS SET AND FOUND IN DATABASE
            echo $this->get_widget_image( null, $columns, $thumbnail_size, $full_width_grid, $default_image_id );
        }
        else {
            // OPTIONS HAS NOT ANY IMAGE USE PLACEHOLDER
            echo '<picture class="'.$thumbnail_size.'">' .
                '<img src="'. $placeholdit_url .'"/>'.
            '</picture>';
        }
    }


    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $defaults = array(
            'order' => 'recent',
            'rows' => 1,
            'columns' => 4,
            'numbers' => '',
            'thumbnail_size' => 'post-thumbnail'
        );

        $instance = wp_parse_args( $instance, $defaults );

        extract( $instance );

        $numeric_id = preg_replace( '/[^\d]/', '', $this->number);
    ?>

        <div id="kehittamo_<?php echo $this->id; ?>" class="kehittamo-posts-grid-widget" data-widget-id="<?php echo $numeric_id; ?>">
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'title_length' ); ?>"><?php _e( 'Title Length:', 'kehittamo-posts-grid' ); ?></label>
            <input class="widefat title_length" id="<?php echo $this->get_field_id( 'title_length' ); ?>" name="<?php echo $this->get_field_name( 'title_length' ); ?>" type="text" value="<?php echo $title_length; ?>"/>
            <small><?php _e( 'Enter the number of characters, after which the title is being cut.', 'kehittamo-posts-grid' ); ?></small>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'excerpt_length' ); ?>"><?php _e( 'Excerpt Length:', 'kehittamo-posts-grid' ); ?></label>
            <input class="widefat excerpt_length" id="<?php echo $this->get_field_id( 'excerpt_length' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_length' ); ?>" type="text" value="<?php echo $excerpt_length; ?>"/>
            <small><?php _e( 'Enter the number of characters, after which the excerpt is being cut.', 'kehittamo-posts-grid' ); ?></small>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php _e( 'Columns:', 'kehittamo-posts-grid' ); ?></label>
            <select class="widefat columns" id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>">
                <option value="1" <?php if ( $columns == 1 ) echo 'selected="selected"'; ?>>1</option>
                <option value="2" <?php if ( $columns == 2 ) echo 'selected="selected"'; ?>>2</option>
                <option value="3" <?php if ( $columns == 3 ) echo 'selected="selected"'; ?>>3</option>
                <option value="4" <?php if ( $columns == 4 ) echo 'selected="selected"'; ?>>4</option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'rows' ); ?>"><?php _e( 'Rows:', 'kehittamo-posts-grid' ); ?></label>
            <select class="widefat rows" id="<?php echo $this->get_field_id( 'rows' ); ?>" name="<?php echo $this->get_field_name( 'rows' ); ?>">
                <option value="1" <?php if ( $rows == 1 ) echo 'selected="selected"'; ?>>1</option>
                <option value="2" <?php if ( $rows == 2 ) echo 'selected="selected"'; ?>>2</option>
                <option value="3" <?php if ( $rows == 3 ) echo 'selected="selected"'; ?>>3</option>
                <option value="4" <?php if ( $rows == 4 ) echo 'selected="selected"'; ?>>4</option>
            </select>
        </p>

        <p>
            <input class="checkbox" type="checkbox" <?php checked($instance['disable_overlay'], 'on' ); ?> id="<?php echo $this->get_field_id( 'disable_overlay' ); ?>" name="<?php echo $this->get_field_name( 'disable_overlay' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'disable_overlay' ); ?>"><?php _e( 'Disable overlay', 'kehittamo-posts-grid' ); ?></label>
        </p>

        <p>
            <input class="checkbox" type="checkbox" <?php checked($instance['full_width_grid'], 'on' ); ?> id="<?php echo $this->get_field_id( 'full_width_grid' ); ?>" name="<?php echo $this->get_field_name( 'full_width_grid' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'full_width_grid' ); ?>"><?php _e( 'This is full width element', 'kehittamo-posts-grid' );?></label>
        </p>

        <p>
          <input class="checkbox" type="checkbox" <?php checked($instance['show_share_counts'], 'on' ); ?> id="<?php echo $this->get_field_id( 'show_share_counts' ); ?>" name="<?php echo $this->get_field_name( 'show_share_counts' ); ?>" />
          <label for="<?php echo $this->get_field_id( 'show_share_counts' ); ?>"><?php _e( 'Show share counts', 'kehittamo-posts-grid' );?></label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'thumbnail_size' ); ?>"><?php _e( 'Thumbnail Size:', 'kehittamo-posts-grid' ); ?></label>
            <select class="widefat thumbnail-size" id="<?php echo $this->get_field_id( 'thumbnail_size' ); ?>" name="<?php echo $this->get_field_name( 'thumbnail_size' ); ?>">
                <option value="wide" <?php selected( $thumbnail_size, 'wide' ); ?>><?php _e( 'Wide (16:9)', 'kehittamo-posts-grid' ); ?></option>
                <option value="square" <?php selected( $thumbnail_size, 'square' ); ?>><?php _e( 'Normal (4:4)', 'kehittamo-posts-grid' ); ?></option>
                <option value="circle" <?php selected( $thumbnail_size, 'circle' ); ?>><?php _e( 'Circle', 'kehittamo-posts-grid' ); ?></option>
                <option value="none" <?php selected( $thumbnail_size, 'none' ); ?>><?php _e( 'None', 'kehittamo-posts-grid' ); ?></option>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'thumbnail_position' ); ?>"><?php _e( 'Thumbnail Position:', 'kehittamo-posts-grid' ); ?></label>
            <select class="widefat thumbnail-position" id="<?php echo $this->get_field_id( 'thumbnail_position' ); ?>" name="<?php echo $this->get_field_name( 'thumbnail_position' ); ?>">
                <option value="top" <?php selected( $thumbnail_position, 'top' ); ?>><?php _e( 'Top', 'kehittamo-posts-grid' ); ?></option>
                <option value="bottom" <?php selected( $thumbnail_position, 'bottom' ); ?>><?php _e( 'Bottom', 'kehittamo-posts-grid' ); ?></option>
                <option value="left" <?php selected( $thumbnail_position, 'left' ); ?>><?php _e( 'Left', 'kehittamo-posts-grid' ); ?></option>
                <option value="right" <?php selected( $thumbnail_position, 'right' ); ?>><?php _e( 'Right', 'kehittamo-posts-grid' ); ?></option>
                <option value="alternately" <?php selected( $thumbnail_position, 'alternately' ); ?>><?php _e( 'Alternately Left and Right', 'kehittamo-posts-grid' ); ?></option>
            </select>
        </p>

        <p>
          <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background color:', 'kehittamo-posts-grid' );?></label>
          <input type="text" class="posts-grid-color-picker widefat" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo $background_color;?>" />
        </p>

        <p>
          <label for="<?php echo $this->get_field_id( 'text_color' ); ?>"><?php _e( 'Text color:', 'kehittamo-posts-grid' );?></label>
          <input type="text" class="posts-grid-color-picker widefat" id="<?php echo $this->get_field_id( 'text_color' ); ?>" name="<?php echo $this->get_field_name( 'text_color' ); ?>" value="<?php echo $text_color;?>" />
        </p>

        <p>
            <input class="checkbox" type="checkbox" <?php checked($instance['hide_date'], 'on' ); ?> id="<?php echo $this->get_field_id( 'hide_date' ); ?>" name="<?php echo $this->get_field_name( 'hide_date' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'hide_date' ); ?>"><?php _e( 'Hide date', 'kehittamo-posts-grid' ); ?></label>
        </p>

        <p>
            <input class="checkbox" type="checkbox" <?php checked($instance['hide_category'], 'on' ); ?> id="<?php echo $this->get_field_id( 'hide_category' ); ?>" name="<?php echo $this->get_field_name( 'hide_category' ); ?>" />
            <label for="<?php echo $this->get_field_id( 'hide_category' ); ?>"><?php _e( 'Hide category', 'kehittamo-posts-grid' ); ?></label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Recent posts:', 'kehittamo-posts-grid' ); ?></label>
            <input type="radio" class="widefat radio order" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" value="recent" <?php checked( $order, 'recent' ); ?>/>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Custom order:', 'kehittamo-posts-grid' ); ?></label>
            <input type="radio" class="widefat radio order" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" value="custom" <?php checked( $order, 'custom' ); ?>/>
        </p>

        <p>
            <input class="widefat" id="ids" name="<?php echo $this->get_field_name( 'numbers' ); ?>" type="hidden" value="<?php echo $numbers; ?>" />
        </p>

        <div class="addform">
        <ul class="posts-container"></ul>

        <p>
            <label for="addnew"><?php _e( 'Add new post:', 'kehittamo-posts-grid' ); ?></label>
            <input id="addnew" type="text" class="widefat add" />
            <small><?php _e( 'Just start typing or press space bar. Remember to save your changes.', 'kehittamo-posts-grid' ); ?></small>
        </p>

        </div>

                    <!-- Category Select Menu -->
        <div class="category-wrapper">
            <p>
                <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:', 'kehittamo-posts-grid' ); ?></label>
                <select id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" class="widefat category" style="width:100%;">
                    <option value=""><?php _e( 'All categories', 'kehittamo-posts-grid' ); ?></option>
                    <?php foreach(get_terms( 'category','hide_empty=0' ) as $term) { ?>
                    <option <?php selected( $category, $term->term_id ); ?> value="<?php echo $term->term_id; ?>"><?php echo ( $term->parent ) ? '- ' : ''; ?><?php echo $term->name; ?></option>
                    <?php } ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id( 'offset' ); ?>"><?php _e( 'Offset:', 'kehittamo-posts-grid' ); ?></label>
                <input class="widefat offset" id="<?php echo $this->get_field_id( 'offset' ); ?>" name="<?php echo $this->get_field_name( 'offset' ); ?>" type="text" value="<?php echo (int) $offset; ?>"/>
                <small><?php _e( 'Number of posts to skip from the start (0-20).', 'kehittamo-posts-grid' ); ?></small>
            </p>
        </div>

        </div>


        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {

        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['background_color'] = ( ! empty( $new_instance['background_color'] ) ) ? strip_tags( $new_instance['background_color'] ) : '';
        $instance['text_color'] = ( ! empty( $new_instance['text_color'] ) ) ? strip_tags( $new_instance['text_color'] ) : '';
        $instance['title_length'] = ( ! empty( $new_instance['title_length'] ) ) ? intval( $new_instance['title_length'] ) : '';
        $instance['excerpt_length'] = ( ! empty( $new_instance['excerpt_length'] ) ) ? intval( $new_instance['excerpt_length'] ) : '';
        $instance['columns'] = ( ! empty( $new_instance['columns'] ) ) ? intval( $new_instance['columns'] ) : 1;
        $instance['rows'] = ( ! empty( $new_instance['rows'] ) ) ? intval( $new_instance['rows'] ) : 1;
        $instance['full_width_grid'] = strip_tags( $new_instance['full_width_grid'] );
        $instance['show_share_counts'] = strip_tags( $new_instance['show_share_counts'] );
        $instance['thumbnail_size'] = strip_tags( $new_instance['thumbnail_size'] );
        $instance['thumbnail_position'] = strip_tags( $new_instance['thumbnail_position'] );
        $instance['hide_date'] = strip_tags( $new_instance['hide_date'] );
        $instance['hide_category'] = strip_tags( $new_instance['hide_category'] );
        $instance['offset'] = ( ! empty( $new_instance['offset'] ) ) ? intval( $new_instance['offset'] ) : 0;
        $instance['category'] = ( ! empty( $new_instance['category'] ) ) ? intval( $new_instance['category'] ) : '';
        $instance['order'] = ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : 'recent';
        $instance['numbers'] = ( ! empty( $new_instance['numbers'] ) ) ? strip_tags( $new_instance['numbers'] ) : 'recent';

        // IF 'Kuvan sijainti' to left/right/alternately then add 'Siirrä teksti kuvan alle' checked
        if( $instance['thumbnail_position'] == 'left' OR $instance['thumbnail_position'] == 'right'  OR $instance['thumbnail_position'] == 'alternately' ) {
            $instance['disable_overlay'] = 'on';
        } else {
            $instance['disable_overlay'] = strip_tags( $new_instance['disable_overlay'] );
        }

        return $instance;
    }

    public function get_posts_by_ids( $numbers ){

        if( ! $numbers )
            return false;

        $posts = array();
        $ids = explode( ', ', $numbers);

        foreach( $ids as $number ){

            $single = array();
            $original = get_post($number);

            //print_r($original);

            if($original->post_status == 'publish' ) {
                $single['title'] = $original->post_title;
                $single['id'] = $original->ID;

                $posts[] = $single;
            }

        }

        return $posts;
    }

    function get_sub_category(){

        if( get_the_category() ){
            foreach(( get_the_category() ) as $category) {
                if( $category->parent ){
                    return $category->name; break;
                }
            }
        }
        else{
            return false;
        }
    }

    function get_the_title( $title_length = '' ){

        if( $title_length > 0 AND mb_strlen( get_the_title() ) >= $title_length ){
            $title = mb_substr( get_the_title(), 0, $title_length ) . '...';
        } else{
            $title = get_the_title();
        }
        return $title;

    }

    function trim( $string = '', $length = '' ){

        if( ! $string )
        return false;

        if( $length > 0 AND mb_strlen( $string ) >= $length ){
            $output = mb_substr( $string, 0, $length ) . '...';
        } else {
            $output = get_the_title();
        }
        return $output;

    }

  /**
   * Get share counts from Facebook and Twitter based on page/post url
   *
   * @param int $id WP post / page id
   * @param string $url WP post / page url
   * @param boolean $exclude_outer_div to exclude or not outer "<div class="grid-block-share">" from returned data
   * @param boolean $get_total_shares_count to get or not only total shares count as a number
   *
   * @return string html markup with sharecounts / sharecount as a number
   */
  public function get_share_counts( $id, $url, $exclude_outer_div, $get_total_shares_count = false ){

    $escaped_url = esc_url( $url );
    if( $escaped_url && $id ) :
      if( $get_total_shares_count && is_string( $total_shares_count_cache = get_transient( 'total_shares_count_' . $id ) )) {
        return $total_shares_count_cache;
      }
      $share_count_cache = $exclude_outer_div ? get_transient( 'share_count_stripped_' . $id ) : get_transient( 'share_count_' . $id );

      if( $share_count_cache && !$get_total_shares_count ) return $share_count_cache;

      $twitter_json = $this->get_data( 'http://cdn.api.twitter.com/1/urls/count.json?url=' . $escaped_url );
      $twitter_obj = json_decode( $twitter_json );
      $twitter_shares = $twitter_obj->count ? $twitter_obj->count : '0';
      $total_share_count = $twitter_shares;

      $facebook_json = $this->get_data( 'https://api.facebook.com/method/links.getStats?format=json&urls=' . $escaped_url );
      $facebook_obj = json_decode( $facebook_json );
      $facebook_obj = is_array( $facebook_obj ) ? $facebook_obj[0] : $facebook_obj;
      $fb_shares = $facebook_obj->share_count ? $facebook_obj->share_count : '0';
      $fb_comments = $facebook_obj->commentsbox_count ? $facebook_obj->commentsbox_count : '0';
      $total_share_count = $total_share_count + $fb_shares;
      $html = '';
      if( !$exclude_outer_div ) $html .= '<div class="grid-block-share">';
        $html .= '<div class="grid-block-share-tw">'. $twitter_shares .'</div>';
        $html .= '<div class="grid-block-share-fb">'. $fb_shares .'</div>';
        $html .= '<div class="grid-block-share-fb-comments">'. $fb_comments .'</div>';
      if( !$exclude_outer_div ) $html .= '</div>';

      // Set 5min cache
      if( $exclude_outer_div ) set_transient( 'share_count_stripped_' . $id, $html, 60 * 5 );
      else set_transient( 'share_count_' . $id, $html, 60 * 5 );
      set_transient( 'total_shares_count_' . $id, $total_share_count, 60 * 5 );

      if( $get_total_shares_count ) return $total_share_count;
      else return $html;

    endif;
  }

  /**
   * Get data from url with curl or file_get_contents
   *
   * @param string $url
   *
   * @return mixed
   */
  private function get_data( $url ){
    if( function_exists( 'curl_version' )) {
      $ch = curl_init();
      curl_setopt( $ch, CURLOPT_USERAGENT, 'getShareCount/0.1 by kehittamo' );
      curl_setopt( $ch, CURLOPT_URL, $url );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
      curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 1 );
      $data = curl_exec( $ch );
      curl_close( $ch );
    }
    else {
      $data = @file_get_contents( $url );
    }

    return $data;
  }

  /**
   * Get post thumbnail as picture element with mobile and desktop + retina versions of images
   *
   * @param int $post_id WP post / page id
   * @param int $columns number of columns used in posts-grid
   * @param string $thumbnail_size wide/square/circle
   * @param boolean $full_width_grid is this widget full width (returns bigger image)
   * @param int $default_img_id attachment id
   *
   * @return string html markup with picture element
   */
  private function get_widget_image( $post_id, $columns, $thumbnail_size, $full_width_grid, $default_img_id = null ){
    // Get different image sizes
    // TODO: use set/get_transient?
    $image_id = $default_img_id ? $default_img_id : get_post_thumbnail_id( $post_id );

    $thumbnail_xs = wp_get_attachment_image_src( $image_id, 'thumbnail-xs' );
    $thumbnail_xs_url = $thumbnail_xs['0'];
    $thumbnail_sm = wp_get_attachment_image_src( $image_id, 'thumbnail-sm' );
    $thumbnail_sm_url = $thumbnail_sm['0'];
    $thumbnail_md = wp_get_attachment_image_src( $image_id, 'thumbnail-md' );
    $thumbnail_md_url = $thumbnail_md['0'];
    $thumbnail_lg = wp_get_attachment_image_src( $image_id, 'thumbnail-lg' );
    $thumbnail_lg_url = $thumbnail_lg['0'];
    $thumbnail_square_xs = wp_get_attachment_image_src( $image_id, 'thumbnail-square-xs' );
    $thumbnail_square_xs_url = $thumbnail_square_xs['0'];
    $thumbnail_square_sm = wp_get_attachment_image_src( $image_id, 'thumbnail-square-sm' );
    $thumbnail_square_sm_url = $thumbnail_square_sm['0'];
    $thumbnail_square_md = wp_get_attachment_image_src( $image_id, 'thumbnail-square-md' );
    $thumbnail_square_md_url = $thumbnail_square_md['0'];
    // Define picture sizes based on number of columns
    if( $columns == 1 ){
      if( $thumbnail_size == 'square' || $thumbnail_size == 'circle' ){
        $full_width_grid ? $picture_size_mobile = $thumbnail_square_md_url : $picture_size_mobile = $thumbnail_square_sm_url;
        $full_width_grid ? $picture_size_mobile_retina = $thumbnail_square_md_url : $picture_size_mobile_retina = $thumbnail_square_sm_url;
        $picture_size_default = $thumbnail_square_md_url;
        $picture_size_retina = $thumbnail_square_md_url;
        $picture_size_big = $thumbnail_square_md_url;
        $picture_size_big_retina = $thumbnail_square_md_url;
      } else{
        $full_width_grid ? $picture_size_mobile = $thumbnail_md_url : $picture_size_mobile = $thumbnail_sm_url;
        $full_width_grid ? $picture_size_mobile_retina = $thumbnail_lg_url : $picture_size_mobile_retina = $thumbnail_md_url;
        $full_width_grid ? $picture_size_default = $thumbnail_lg_url : $picture_size_default = $thumbnail_md_url;
        $picture_size_retina = $thumbnail_lg_url;
        $picture_size_big = $thumbnail_lg_url;
        $picture_size_big_retina = $thumbnail_lg_url;
      }
    } elseif( $columns == 2 ){
      if( $thumbnail_size == 'square' || $thumbnail_size == 'circle' ){
        $picture_size_mobile = $thumbnail_square_xs_url;
        $picture_size_mobile_retina = $thumbnail_square_sm_url;
        $picture_size_default = $thumbnail_square_sm_url;
        $picture_size_retina = $thumbnail_square_md_url;
        $picture_size_big = $thumbnail_square_md_url;
        $picture_size_big_retina = $thumbnail_square_md_url;
      } else{
        $picture_size_mobile = $thumbnail_xs_url;
        $picture_size_mobile_retina = $thumbnail_sm_url;
        $picture_size_default = $thumbnail_sm_url;
        $picture_size_default = $thumbnail_sm_url;
        $picture_size_retina = $thumbnail_md_url;
        $picture_size_big = $thumbnail_lg_url;
        $picture_size_big_retina = $thumbnail_lg_url;
      }
    } elseif( $columns == 3 OR $columns == 4){
      if( $thumbnail_size == 'square' || $thumbnail_size == 'circle' ){
        $picture_size_mobile = $thumbnail_square_xs_url;
        $picture_size_mobile_retina = $thumbnail_square_sm_url;
        $picture_size_default = $thumbnail_square_xs_url;
        $picture_size_retina = $thumbnail_square_sm_url;
        $picture_size_big = $thumbnail_square_sm_url;
        $picture_size_big_retina = $thumbnail_square_md_url;
      } else{
        $picture_size_mobile = $thumbnail_xs_url;
        $picture_size_mobile_retina = $thumbnail_sm_url;
        $picture_size_default = $thumbnail_xs_url;
        $picture_size_retina = $thumbnail_sm_url;
        $picture_size_big = $thumbnail_sm_url;
        $picture_size_big_retina = $thumbnail_lg_url;
      }
    }

    $image = '<picture class="'.$thumbnail_size.'">';
      $image .= '<source data-srcset="'. $picture_size_big . ', ' . $picture_size_big_retina .' 2x" srcset="'. $picture_size_big . ', ' . $picture_size_big_retina .' 2x" media="(min-width: 1200px)"/>';
      $image .= '<source data-srcset="'. $picture_size_default . ', ' . $picture_size_retina .' 2x" srcset="'. $picture_size_default . ', ' . $picture_size_retina .' 2x" media="(min-width: 768px)"/>';
      $image .= '<source data-srcset="'. $picture_size_mobile . ', ' . $picture_size_mobile_retina .' 2x" srcset="'. $picture_size_mobile . ', ' . $picture_size_mobile_retina .' 2x"/>';
      $image .= '<img class="responsively-lazy" data-src="'. $picture_size_default .'" src="'. $picture_size_default .'"/>';
    $image .= '</picture>';
    return $image;
  }

    /**
   * Return an ID of an attachment by searching the database with the file URL.
   *
   * First checks to see if the $url is pointing to a file that exists in
   * the wp-content directory. If so, then we search the database for a
   * partial match consisting of the remaining path AFTER the wp-content
   * directory. Finally, if a match is found the attachment ID will be
   * returned.
   *
   * @param string $url The URL of the image (ex: http://mysite.com/wp-content/uploads/2013/05/test-image.jpg)
   *
   * @return int|null $attachment Returns an attachment ID, or null if no attachment is found
   * @copyright http://frankiejarrett.com/2013/05/get-an-attachment-id-by-url-in-wordpress/
   */
  private function get_attachment_id_by_url( $url ) {
    // Split the $url into two parts with the wp-content directory as the separator
    $parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
    // Get the host of the current site and the host of the $url, ignoring www
    $this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
    $file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
    // Return nothing if there aren't any $url parts or if the current host and $url host do not match
    if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
        return;
    }
    // Now we're going to quickly search the DB for any attachment GUID with a partial path match
    // Example: /uploads/2013/05/test-image.jpg
    global $wpdb;
    $attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );
    // Returns null if no attachment is found
    return $attachment[0];
  }

  /**
   * Get color rgb value from hex
   *
   * @param string $hex color hex-value
   * @param string $alpha return alpha value (rgba) with given $alpha value
   *
   * @return string the rgb value
   * @source http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
   */
  private function hex2rgb( $hex , $alpha = false ) {

    $hex = str_replace( "#", "", $hex );

    if( !ctype_xdigit ($hex ) ) return null;

    if( strlen( $hex ) == 3 ) {
      $r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
      $g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
      $b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
    } else {
      $r = hexdec( substr( $hex, 0, 2 ) );
      $g = hexdec( substr( $hex, 2, 2 ) );
      $b = hexdec( substr( $hex, 4, 2 ) );
    }
    $rgb = array( $r, $g, $b );

    if( $alpha && is_numeric( $alpha ) && 1 >= $alpha ) return 'rgba( ' . implode( ",", $rgb ) . ', '. $alpha .' )';
    return 'rgb( ' . implode( ",", $rgb ) . ' )';
 }

    /**
     * Get video embed code from Advanced Custom Field in Article
     *
     * @return string the embed html
     */
    private function get_video_embed() {

        // Check that ACF is working properly and the video_embed field exists and has a value. If not, return false.
        if ( function_exists( 'get_field' ) && get_field( "video_embed" ) ) {

            // Escape maliciousness
            $video_share_url = esc_url( get_field( 'video_embed' ) );

            // Check that the url is the standard YouTube share url and determine the video ID. If not, return false.
            if ( strpos( $video_share_url, 'https://youtu.be/' ) !== false ) {

                $exploded_url = explode( '/', $video_share_url );
                $embed_code = $exploded_url[3];

                // Embed the video in an iframe with YouTube's recommended HTML
                return '<div class="embed-container"><iframe src="https://www.youtube.com/embed/' . $embed_code . '" frameborder="0" allowfullscreen></iframe></div>';

            } else {

                return false;

            }

        } else {

            return false;

        }

    }

} // class Posts_Grid_Widget
